package com.juntos.quizes.canadaDay.events {

    import flash.events.Event;
    
    /**
     *@author ghostmonk - 2009-06-25
     */
    public class AnswerEvent extends Event {
        
        public static const ANSWER_SELECTION:String = "answerSelection";
        public static const QUIZ_COMPLETE:String = "quizComplete";
        public static const DISABLE_SELECTION:String = "disableSelection";
        
        private var _value:int;
        private var _isCorrect:Boolean;
        
        
        public function get isCorrect():Boolean {
        	return _isCorrect;
        }
        
        
        public function get value():int {
        	
        	return _value;
        	
        }
        
        

        public function AnswerEvent( type:String, value:int, isCorrect:Boolean = false, bubbles:Boolean=false, cancelable:Boolean=false ) {
        	
        	_value = value;
        	_isCorrect = isCorrect;
        	super( type, bubbles, cancelable );
        
        }
        
        
        
        override public function clone() : Event {
        	
        	return new AnswerEvent( type, value, isCorrect, bubbles, cancelable );
        	
        }
        
        
        
    }
}