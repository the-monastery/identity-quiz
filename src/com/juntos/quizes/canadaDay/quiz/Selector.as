package com.juntos.quizes.canadaDay.quiz {

    import com.ghostmonk.events.RadioSelectionEvent;
    import com.ghostmonk.ui.formControls.RadioSelector;
    import com.juntos.quizes.canadaDay.events.AnswerEvent;
    import com.juntos.quizes.canadaDay.model.Canadian;
    
    import flash.display.Sprite;
    import flash.text.TextField;
    
    [Event(name="answerSelection", type="com.juntos.quizes.canadaDay.events.AnswerEvent")]
    
    /**
     *@author ghostmonk - 2009-06-24
     */
    public class Selector extends Sprite {
    
        private var _canadian:Canadian;   
        private var _radioBtn:RadioSelector;
        private var _field:TextField;
        private var _isPermDisabled:Boolean;
        private var _id:int;
        
        
        public function get id():int {
        	return _id;
        }
        
        public function get isPermDisabled():Boolean {
        	return _isPermDisabled;
        }
        
        
        public function Selector( item:Canadian, radioBtn:RadioSelector, field:TextField ) {
            
            _id = item.index;
            
            _canadian = item;
            _radioBtn = radioBtn;
            _field = field;
            _isPermDisabled = false;
            
            _field.text = _canadian.name;
            _field.x = int( radioBtn.width + 1 );
            
            _radioBtn.y = _radioBtn.x = int( _radioBtn.width * 0.5 );
            _radioBtn.y += 4;
            
            addChild( _radioBtn );
            addChild( _field );
            
            _radioBtn.addEventListener( RadioSelectionEvent.RADIO_SELECTION, onRadioSelection );
            
        }
        
        
        public function enable():void {
            
            if( !_isPermDisabled ) {
                _radioBtn.enable();
                alpha = 1;
            }
            
        }
        
        
        public function disable():void {
        	
        	_radioBtn.disable();
        	alpha = 0.4;
        	
        }
        
        
        public function permDisable( isCorrect:Boolean ):void {
        	
        	if( isCorrect ) {
        		_radioBtn.addChild( new CheckAsset() );
        	}
        	else {
        		_radioBtn.addChild( new XAsset() );
        	}
        	
        	_isPermDisabled = true;
        	_radioBtn.isChecked = false;
        	disable();
        	
        }
        
        
        private function onRadioSelection( e:RadioSelectionEvent ):void {
        	
        	dispatchEvent( new AnswerEvent( AnswerEvent.ANSWER_SELECTION, e.id ) );
        	
        }
         
        
    }
}