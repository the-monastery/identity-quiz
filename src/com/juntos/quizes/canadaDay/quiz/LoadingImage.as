package com.juntos.quizes.canadaDay.quiz {

    import caurina.transitions.Equations;
    import caurina.transitions.Tweener;
    
    import com.ghostmonk.net.AssetLoader;
    
    import flash.display.Bitmap;
    import flash.display.Sprite;
    import flash.filters.DropShadowFilter;
    
    /**
     *@author ghostmonk - 2009-06-24
     */
    public class LoadingImage extends Sprite {
        
        private var _assetLoader:AssetLoader;
        
        public function LoadingImage( url:String ) {
        	
        	_assetLoader = new AssetLoader( url, imageLoaded );
        	filters = [ new DropShadowFilter(5, 45, 0x454545, 0.5, 5, 5, 1, 2 ) ];
        	scaleX = scaleY = 0;
        	
        }
        
        public function buildIn():void {
        	
        	Tweener.addTween(this, {scaleX:1, scaleY:1, alpha:1, transition:Equations.easeOutBack, time:0.3});	
        	
        }
        
        public function buildOut():void {
        	
        	Tweener.addTween(this, {scaleX:0, scaleY:0, alpha:0, time:0.3, transition:Equations.easeInBack, onComplete:buildOutComplete});
        	
        }
        
        private function buildOutComplete():void {
        	
        	if( parent ) {
        		parent.removeChild( this );
        	}
        	
        }
        
        private function imageLoaded( bitmap:Bitmap ):void {
        	
        	bitmap.smoothing = true;
        	bitmap.x = -bitmap.width/2;
            bitmap.y = -bitmap.height/2;
        	addChild( bitmap );
        	
        }
        
    }
}