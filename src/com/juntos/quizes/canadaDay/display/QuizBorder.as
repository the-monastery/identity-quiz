package com.juntos.quizes.canadaDay.display {
	
	import caurina.transitions.Tweener;
	
	import flash.events.Event;
	import flash.filters.GlowFilter;
    
    [ Event ( name="complete", type="flash.events.Event" ) ]
    
    /**
     *@author ghostmonk - 2009-06-24
     */
    public class QuizBorder extends QuizBorderAssets {
    	
    	private const BLUR:GlowFilter = new GlowFilter( 0x000000, 0.5, 23, 23, 1, 5 );
    	private var _callback:Function;
    	
        public function QuizBorder( callBack:Function ) {
            
            _callback = callBack;
            
            scaleX = scaleY = 0;
            filters = [ BLUR ];
            
            addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
            
        }
        
        
        
        private function onAddedToStage( e:Event ):void {
        	
        	removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
        	
        	x = stage.stageWidth * 0.5;
        	y = stage.stageHeight * 0.5;
        	
        	buildIn();
        	
        }
        
        
        
        private function buildIn():void {
        	
        	Tweener.addTween( this, { scaleX:1, scaleY:1, y:0, x:0, time:0.5, delay:0.5, onComplete:_callback } );
                
        }
        
        
        
    }
}