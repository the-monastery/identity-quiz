package com.juntos.quizes.canadaDay.model {
	

    /**
     *@author ghostmonk - 2009-06-24
     */
    public class Key {
        
        private const _basePictureURL:String = "quizData/images/";
        
        private const _answersHash:Array = [
        
            new Canadian( 'null', 0 ),
            new Canadian( 'null', 1 ),
            new Canadian( 'null', 2 ),
            new Canadian( 'null', 3 ),
            new Canadian( 'null', 4 ),
            new Canadian( 'null', 5 ),
            new Canadian( 'null', 6 ),
            new Canadian( 'null', 7 ),
            new Canadian( 'null', 8 ),
            new Canadian( 'null', 9 ),
            new Canadian( '01.jpg', 10 ),
            new Canadian( '02.jpg', 11 ),
            new Canadian( '03.jpg', 12 ),
            new Canadian( '04.jpg', 13 ),
            new Canadian( '05.jpg', 14 ),
            new Canadian( '06.jpg', 15 ),
            new Canadian( '07.jpg', 16 ),
            new Canadian( '08.jpg', 17 ),
            new Canadian( '09.jpg', 18 ),
            new Canadian( '10.jpg', 19 ),
            new Canadian( '11.jpg', 20 ),
            new Canadian( '12.jpg', 21 ),
            new Canadian( '13.jpg', 22 ),
            new Canadian( '14.jpg', 23 ),
            new Canadian( '15.jpg', 24 ),
            new Canadian( '16.jpg', 25 ),
            new Canadian( '17.jpg', 26 ),
            new Canadian( '18.jpg', 27 ),
            new Canadian( '19.jpg', 28 ),
            new Canadian( '20.jpg', 29 )
        
        ]; 
        
        public function addName( index:int, name:String ):void {
        	
        	( _answersHash[ index ] as Canadian ).name = name;
        	
        }
        
        public function get answerHash():Array {
        
            return _answersHash;
            
        }
        
        public function get basePicURL():String {
        	
        	return _basePictureURL;
        	
        }
        
        public function toString():String {
        	
        	var output:String = "";
        	
        	for each( var item:Canadian in _answersHash ) {
        		output += "\n" + item.toString();
        	}
        	
        	return output;
        	
        }
        
    }
}