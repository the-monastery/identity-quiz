package com.juntos.quizes.canadaDay.model {

    /**
     *@author ghostmonk - 2009-06-24
     */
    public class Canadian {


    	
    	private var _name:String;
    	private var _image:String;
    	private var _index:int;

        
        public function Canadian( image:String, answerID:int ){
        	
        	_image = image;
        	_index = answerID;
        	
        }
        
        
    	public function get index():int {
    		
    		return _index;
    		
    	}



    	public function get image():String {
    		
    		return _image;
    		
    	}


    
    	public function get name():String {
    		
    		return _name;
    		
    	}
    	
    	
    	
    	public function set name( value:String ):void {
    		
    		_name = value;
    		
    	}
    	
    	
    	public function toString():String {
    		
    		return "Name: " + _name +
    		  "\nIndex: " + _index + 
    		  "\nImage: " + _image;
    		
    	}



    }
}