package com.juntos.quizes.canadaDay.events {

    import com.juntos.quizes.canadaDay.model.UserInfo;
    
    import flash.events.Event;
    
    /**
     *@author ghostmonk - 2009-06-24
     */
    public class FormEvent extends Event {
        
        public static const FORM_SUBMIT:String = "formSubmit";
        
        private var _userInfo:UserInfo;
        
        public function get userInfo():UserInfo {
        	return _userInfo;
        }
        
        public function FormEvent( type:String, userInfo:UserInfo, bubbles:Boolean=false, cancelable:Boolean=false ) {
        	
        	_userInfo = userInfo;
        	super(type, bubbles, cancelable);
        	
        }
        
        
        override public function clone() : Event {
        	
        	return new FormEvent( type, userInfo, bubbles, cancelable );
        	
        }
        
    }
}