package com.juntos.quizes.canadaDay.quiz {

    import caurina.transitions.Equations;
    import caurina.transitions.Tweener;
    
    import com.ghostmonk.ui.formControls.RadioSelector;
    import com.juntos.quizes.canadaDay.events.AnswerEvent;
    import com.juntos.quizes.canadaDay.model.Canadian;
    import com.juntos.quizes.canadaDay.model.Key;
    
    import flash.display.Sprite;
    
    [Event(name="answerSelection", type="com.juntos.quizes.canadaDay.events.AnswerEvent")]
    
    /**
     *@author ghostmonk - 2009-06-24
     */
    public class SelectorGrid extends Sprite {
        
        
        
        private var _selections:Array;
        private var _yPosTracker:int;
        
        
        
        public function SelectorGrid( key:Key, radioRadius:Number ) {
        	
        	_selections = new Array();
        	_yPosTracker = 0;
        	
        	for( var i:int = 0; i < key.answerHash.length; i++ ) {
        		createAndPositionSelector( i, key.answerHash[ i ], radioRadius );
        	}
        	
        	
        }
        
        
        public function disableSelector( id:int, isCorrect:Boolean ):void {
        	
        	for each( var selector:Selector in _selections ) {
        		if( selector.id == id ) {
        			selector.permDisable( isCorrect );
        		}
        		else {
        			selector.enable();
        		}
        	}
        	
        }
        
        
        private function createAndPositionSelector( index:int, canadian:Canadian, radioRadius:int ):void {
        	
            var radio:RadioSelector = new RadioSelector( index, radioRadius );
            var selector:Selector = new Selector( canadian, radio, new SelectorLabelAsset().label );
            selector.addEventListener( AnswerEvent.ANSWER_SELECTION, onSelection );
            
            _selections.push( selector );
            
            selector.x = int( ( index % 3 ) * ( selector.width + 1 ) );
            selector.y = _yPosTracker;
            
            if( index % 3 == 2 ) {
            	_yPosTracker += selector.height + 10;
            }
            
            addChild( selector );
            shuffleGrid();
        	
        }
        
        
        
        public function disable():void {
        	
        	mouseChildren = false;
        	Tweener.addTween( this, { alpha:0.4, time:0.3, transition:Equations.easeNone} );
        	
        }
        
        
        
        public function enable():void {
        	
        	mouseChildren = true;
        	Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone} );
        	
        }
        
        
        
        private function shuffleGrid():void {
        	
        	var iterations:int = _selections.length * 2
        	
        	for( var i:int = 0; i < iterations; i++ ) {
        		swapPositions( _selections[ randomIndex ], _selections[ randomIndex ] ); 
        	}
        	
        }
        
        
        
        private function get randomIndex():int {
        	
        	return Math.floor( Math.random() * _selections.length );
        	
        }
        
        
        
        private function swapPositions( item1:Selector, item2:Selector ) :void {
        	
        	var yLoc1:int = item1.y;
        	var yLoc2:int = item2.y;
        	var xLoc1:int = item1.x;
        	var xLoc2:int = item2.x;
        	
        	item1.x = xLoc2;
        	item1.y = yLoc2;
        	
        	item2.x = xLoc1;
        	item2.y = yLoc1;
        	
        }
        
        
        
        private function onSelection( e:AnswerEvent ):void {
        	
        	for each( var select:Selector in _selections ) {
        		
        		if( !select.isPermDisabled && select != e.target ) {
        			select.enable();
        		}
        		
        	}
        	
        	( e.target as Selector ).disable();
        	dispatchEvent( e );
        	
        }
        
        
        
    }
}