package com.juntos.quizes.canadaDay.events {

    import flash.events.Event;
    
    /**
     *@author ghostmonk - 2009-06-26
     */
    public class QuizEvent extends Event {
        
        
        public static const NEXT_QUESTION:String = "nextQuestion";
        public static const SUBMIT_ANSWER:String = "submitQuestion";
        
        
        public function QuizEvent( type:String, bubbles:Boolean=false, cancelable:Boolean=false ) {
        	
        	super( type, bubbles, cancelable );
        	
        }
        
        
        override public function clone() : Event {
        	
        	return new QuizEvent( type, bubbles, cancelable );
        	
        }
        
    }
}