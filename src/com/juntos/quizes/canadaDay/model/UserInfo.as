package com.juntos.quizes.canadaDay.model {

    /**
     *@author ghostmonk - 2009-06-24
     */
    public class UserInfo {
    	
    	
    	
    	private var _firstName:String;
    	private var _lastName:String;
    	private var _email:String;
    	private var _age:String;
    	private var _postalCode:String;
    	
        
        
    	public function get firstName():String {

    		return _firstName;

    	}

    	public function set firstName( v:String ):void {

    		_firstName = v;

    	}
    	
    	

    	public function get lastName():String {

    		return _lastName;

    	}

    	public function set lastName( v:String ):void {

    		_lastName = v;

    	}
    	
    	

    	public function get email():String {

    		return _email;

    	}

    	public function set email( v:String ):void {

    		_email = v;

    	}
    	
    	

    	public function get age():String {

    		return _age;

    	}

    	public function set age( v:String ):void {

    		_age = v;

    	}



    	public function get postalCode():String {

    		return _postalCode;

    	}

    	public function set postalCode( v:String ):void {

    		_postalCode = v;

    	}
    	
    	
    	
    	public function toString():String {
    		
    		return "First: " + _firstName +
                "\nLast: " + _lastName +
                "\nEmail: " + _email +
                "\nAge: " + _age +
                "\nPostalCode: " + _postalCode;
        
    		
    	}
    	
    	

    }
}