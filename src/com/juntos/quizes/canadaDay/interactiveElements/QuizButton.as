package com.juntos.quizes.canadaDay.interactiveElements {
	
	import com.juntos.quizes.canadaDay.events.QuizEvent;
	
	import flash.events.MouseEvent;

    [Event (name="nextQuestion", type="com.juntos.quizes.canadaDay.events.QuizEvent")]
    [Event (name="submitAnswer", type="com.juntos.quizes.canadaDay.events.QuizEvent")]
    
    /**
     *@author ghostmonk - 2009-06-26
     */
    public class QuizButton extends NavigationButton {
    	
    	private var _submitLabel:String;
    	private var _nextLabel:String;
    	private var _type:String;
    	
        public function QuizButton( view:NavButtonAsset, submitLabel:String, nextLabel:String ) {
        	
        	_type = QuizEvent.SUBMIT_ANSWER;
        	_submitLabel = submitLabel;
        	_nextLabel = nextLabel;
        	super( view, onClick, submitLabel );
        	buildOut();
        	disable();
        	
        }
        
        
        private function flipType():void {
        	
        	if( _type == QuizEvent.SUBMIT_ANSWER ) {
        		setType( _nextLabel, QuizEvent.NEXT_QUESTION );
        	}
        	else {
        		setType( _submitLabel, QuizEvent.SUBMIT_ANSWER );
        	}
        }
        
        
        private function setType( label:String, evenType:String ):void {
        	
        	_type = evenType;
            view.label.text = label;
            createBitmapText();
        	
        }
        
        
        private function onClick( e:MouseEvent ):void {
        	
        	dispatchEvent( new QuizEvent( _type ) );
        	flipType();
        	
        } 
        
        
        
    }
}