package {

    import caurina.transitions.Tweener;
    
    import com.ghostmonk.form.model.parseMethods.FormLocalizationXML;
    import com.ghostmonk.net.XMLLoader;
    import com.juntos.quizes.canadaDay.display.Background;
    import com.juntos.quizes.canadaDay.display.QuizBorder;
    import com.juntos.quizes.canadaDay.events.AnswerEvent;
    import com.juntos.quizes.canadaDay.events.FormEvent;
    import com.juntos.quizes.canadaDay.events.QuizEvent;
    import com.juntos.quizes.canadaDay.form.UserForm;
    import com.juntos.quizes.canadaDay.model.Key;
    import com.juntos.quizes.canadaDay.quiz.Quiz;
    import com.juntos.quizes.canadaDay.quiz.SelectorGrid;
    
    import flash.display.Sprite;
    import flash.filters.GlowFilter;
    import flash.system.Security;
    
    
    
    /**
     * 
     * @author ghostmonk - 24/08/2009
     * 
     */
    public class CanadaDayQuiz extends Sprite {
        
        
        
        public static const DEBUG:Boolean = false;
        private const CONFIG_DIR:String = "CanadaDayQuiz/quizData/";
        
        private var _xmlLoader:XMLLoader;
        private var _lang:String;
        private var _userForm:UserForm;
        private var _selectorGrid:SelectorGrid;
        private var _border:QuizBorder;
        private var _quiz:Quiz;
        private var _endpoint:String;
        
        private var _key:Key;
        
        private var _initBuildFinished:Boolean;
        
        
        public function init( lang:String ):void {
        	
            _key = new Key();
            _lang = lang;
            _xmlLoader = new XMLLoader( CONFIG_DIR + "data_" + _lang + ".xml", onXMLComplete );
            
            _initBuildFinished = false;
            _border = new QuizBorder( initBuildInComplete );
            addChild( new Background( new Repeater( 0, 0 ) ) );
            addChild( _border );
        	
        }
        
        
        
        private function onXMLComplete( data:XML ):void {
        	
        	fillKey( data.canadians[ 0 ].canadian );
        	
        	_endpoint = data.endpoint;
        	
        	_userForm = new UserForm( 
                new FormLocalizationXML( data.formStrings[ 0 ] ).formStrings,
                [ new GlowFilter( 0xE2D097, 0.7, 10, 10, 2, 4 ) ],
                _endpoint,
                data.btn.@submit 
            )
            
            _userForm.addEventListener( FormEvent.FORM_SUBMIT, onFormSubmit );
            
            _userForm.x = ( stage.stageWidth - _userForm.width ) * 0.5;
            _userForm.y = ( stage.stageHeight - _userForm.height ) * 0.5;
            
            _selectorGrid = new SelectorGrid( _key, 7.5 );
            
            _quiz = new Quiz( _key, CONFIG_DIR, data.quizStrings[ 0 ], data.btn[ 0 ] );
            _quiz.addEventListener( AnswerEvent.QUIZ_COMPLETE, onQuizComplete );
            _quiz.addEventListener( AnswerEvent.DISABLE_SELECTION, disableSelector );
            _quiz.addEventListener( QuizEvent.SUBMIT_ANSWER, onQuizInteraction );
            _quiz.addEventListener( QuizEvent.NEXT_QUESTION, onQuizInteraction );
            
        	initBuildInComplete();
        	
        }
        
        
        
        private function initBuildInComplete():void {
        	
        	if( _initBuildFinished ) {    		
            	addChild( _userForm );
            	_userForm.buildIn();
        	}
        	else {
        		_initBuildFinished = true;
        	}
        	
        }
        
        
        
        private function onFormSubmit( e:FormEvent ):void {
            
            var dest:int = stage.stageHeight - _selectorGrid.height - 40;
            
            _selectorGrid.x = 18;
            _selectorGrid.y = stage.stageHeight;
            _selectorGrid.addEventListener( AnswerEvent.ANSWER_SELECTION, onAnswerSelection );
            addChildAt( _selectorGrid, 1 );
            
            Tweener.addTween( _selectorGrid, { y:dest, time:0.5 } );
            
            addChild( _quiz );
            _quiz.startSubmitURL( _endpoint, e.userInfo.email );
            
        }
        
        
        
        private function fillKey( canadians:XMLList ):void {
        	
        	for( var i:int = 0; i < canadians.length(); i++ ) {
        		_key.addName( i, canadians[ i ].@name );
        	}
        	
        }
        
        
        
        private function onAnswerSelection( e:AnswerEvent ):void {
        	
        	_quiz.choice = e.value;
        	
        }
        
        
        private function onQuizComplete( e:AnswerEvent ):void {
        	
        	Tweener.addTween( _selectorGrid, { y:stage.stageHeight, time:0.5, onComplete:removeChild, onCompleteParams:[_selectorGrid] } );
        	
        }
        
        
        private function onQuizInteraction( e:QuizEvent ):void {
        	
        	if( e.type == QuizEvent.NEXT_QUESTION ) {
        		_selectorGrid.enable();
        	}
        	else {
        		_selectorGrid.disable();
        	}
        	
        }
        
        
        private function disableSelector( e:AnswerEvent ):void {
        	
        	_selectorGrid.disableSelector( e.value, e.isCorrect );
        	
        }
        
        
        
    }
}