package com.juntos.quizes.canadaDay.display {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;

    /**
     *@author ghostmonk - 2009-06-24
     */
    public class Background extends BackgroundAsset {
        
        
        
        private var _fillLayer:Sprite;
        private var _fillmask:Sprite;
        
        
        
        public function Background( fill:BitmapData ) {
           
           alpha = 0; 
           assetCreationSequence( fill );
           addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
                        
        }
        
        
        
        public function buildIn():void {
            
            Tweener.addTween( this, { alpha:1, time:0.5, delay:0.5, transition:Equations.easeNone } );
            
        }
        
        
        
        private function onAddedToStage( e:Event ):void {
            
            removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
            buildIn();
            
        }
        
        
        
        private function assetCreationSequence( fill:BitmapData ):void {
        	
        	_fillLayer = new Sprite();
            _fillmask = new Sprite();
            
            createFill( fill );
            createGradMask();
            addChild( _fillLayer );
            addChild( _fillmask );
            
            _fillmask.cacheAsBitmap = true;
            _fillLayer.cacheAsBitmap = true;
            _fillLayer.mask = _fillmask;
        	
        }
        
        
        
        private function createFill( fill:BitmapData ):void {
        	
        	_fillLayer.graphics.beginBitmapFill( fill );
            _fillLayer.graphics.drawRect( 0, 0, width, height );
            _fillLayer.graphics.endFill();
            _fillLayer.alpha = 0.5;
        	
        }
        
        
        
        private function createGradMask():void {
            
            var matrix:Matrix = new Matrix();
            matrix.createGradientBox( width, height, -1.5 );
            
            _fillmask.graphics.beginGradientFill( GradientType.LINEAR, [0x000000, 0xFFFFFF], [1,0], [50,255], matrix );
            _fillmask.graphics.drawRect( 0, 0, width, height );
            _fillmask.graphics.endFill();
                   
        }
        
        
        
    }
}