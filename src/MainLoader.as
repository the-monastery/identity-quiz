package {

    import com.ghostmonk.net.AssetLoader;
    import com.ghostmonk.utils.SiteLoader;
    
    import flash.display.Sprite;

    [SWF (width=500, height=600, pageTitle="Canada Day Quiz", frameRate=31, backgroundColor=0xFFFFFF)]
    
    /**
     *@author ghostmonk - 2009-06-27
     */
    public class MainLoader extends Sprite {
        
        private var _siteLoader:AssetLoader;
        private var _progressMeter:PreloaderAsset;
        
        public function MainLoader() {
        	
        	_progressMeter = new PreloaderAsset();
        	_siteLoader = new AssetLoader( "CanadaDayQuiz.swf", fullyLoaded, null, _progressMeter );
        	
        	addChild( _progressMeter );
        	_progressMeter.x = ( stage.stageWidth - _progressMeter.width ) * 0.5;
        	_progressMeter.y = ( stage.stageHeight - _progressMeter.height ) * 0.5;
        	
        }
        
        
        private function fullyLoaded( site:* ):void {
        	
        	site.init( root.loaderInfo.parameters.lang == "fr" ? "fr" : "en" );
        	addChild( site );
        	removeChild( _progressMeter );
        	
        }
        
    }
}