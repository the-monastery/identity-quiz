package com.juntos.quizes.canadaDay.interactiveElements {
	
	import caurina.transitions.Tweener;
	
	import flash.display.*;
	import flash.events.*;
	
	public class NavigationButton extends EventDispatcher {
		
		private var _view:NavButtonAsset;
		
		private var _textBitmap:Bitmap;
		private var _currentEventType:String;
		
		private var _callback:Function;
		
		
		
		public function get isEnabled():Boolean {
			
			return _view.buttonMode;
			
		}
		
		
		
		public function get view():NavButtonAsset {
			
			return _view;
			
		}
		
		
		
		public function NavigationButton( view:NavButtonAsset, callBack:Function, label:String ) {
			
			_view = view;
			_callback = callBack;
			
			view.label.text = label;
			
			_textBitmap = new Bitmap();
			_view.mouseChildren = false;
			createBitmapText();
			enable();
			
		}
		
		
		public function buildIn():void {
			
            Tweener.addTween( _view, { alpha:1, x:_view.x - 50, time:0.5 } );
            			
		}
		
		public function buildOut():void {
			
            Tweener.addTween( _view, { alpha:0, x:_view.x + 50, time:0.5 } );
			
		}
		
		
		public function enable():void {
			
			_view.buttonMode = true;
			_view.addEventListener(MouseEvent.CLICK, onMouseClick);
			
		}
		
		
		
		public function disable():void {
			
			_view.buttonMode = false;
			_view.removeEventListener( MouseEvent.CLICK, onMouseClick );
			buildOut();
			
		}
		
		
		
		protected function createBitmapText():void {
			
			_view.addChild( _view.label );
			
			_textBitmap.bitmapData = null;
			_textBitmap.bitmapData = new BitmapData( _view.label.width, _view.label.height, true, 0 );
			_textBitmap.bitmapData.draw( _view.label );
			
			_textBitmap.x = int( _view.label.x );
			_textBitmap.y = int( _view.label.y );
			
			_view.addChild( _textBitmap );
			_view.removeChild( _view.label );
			
		}
		
		
		
		private function onMouseClick( e:MouseEvent ):void {
			
			_callback( e );
			
		}
		

	}
}